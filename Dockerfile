FROM golang:1.13

RUN apt-get update
RUN apt-get install -y curl make awscli tar jq crossbuild-essential-armel crossbuild-essential-armhf
